<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\App;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MotorbikeTest extends TestCase
{
    use DatabaseMigrations;
    protected  $user;
    /**
     * A basic test example.
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(\App\User::class)->make();
    }
    public function a_user_can_create_motorbike()
    {
        $motorbike= factory(\App\Motorbike::clas)->create();
         $this->actingAs($this->user)
             ->get(route('home'))
             ->assertStatus(200)
             ->assertSeeText($motorbike->model)
             ->assertSeeText($motorbike->color);
    }

}
