<?php

namespace Tests\Feature;

use App\Modules\Event\Event;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventTest extends TestCase
{
    use DatabaseMigrations;
    protected $user;

    /**
     * A basic test example.
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(\App\User::class)->create();
    }

    public function a_user_can_see_event_details()
    {
        $event = factory(Event::class)->create();
        $this->actingAs($this->user)
            ->get(route('event.show'))
            ->assertSeeText($event->name)
            ->assertSeeText($event->user->name);
    }
    public function a_user_no_create_event()
    {
        $event = factory(Event::class)->create();
        $this->actingAs($this->user)
            ->get(route('event.show', $event->id))
            ->assertSeeText($event->name)
            ->assertSeeText($event->user->name);
    }

    public function a_user_can_see_events()
    {
        $this->get(route('event.index'))
            ->assertStatus(200);

    }

}
