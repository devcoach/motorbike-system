<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MotorbikeController@index')->name('motorbike.list');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('motorbike','MotorbikeController',[
    'only'=>['store','show']
]);
Route::resource('event','Event\EventController');
