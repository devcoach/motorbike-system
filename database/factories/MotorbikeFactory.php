<?php

use Faker\Generator as Faker;

$factory->define(App\Motorbike::class, function (Faker $faker) {
    return [
        //
        'model'=>$faker->name,
        'color'=>$faker->colorName,
        'weight'=>$faker->numberBetween(800,5000),
        'price'=>$faker->numberBetween(2500,50000),
        'image'=>$faker->name
    ];
});

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});