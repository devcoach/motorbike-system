<?php

use Illuminate\Database\Seeder;

class MotorbikeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Motorbike::class, 30)->create();

    }
}
