@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(count($upcomingEvents) ===0)
                    <div class="alert alert-warning text-capitalize text-center">
                        Empty Upcommig Events
                    </div>
                    @else
                    <div class="card ">
                        <div class="card-header">{{ __(' Upcoming Event Lists') }}</div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Address</th>
                                    <th>Start Date</th>
                                    <th>By</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($upcomingEvents as $event)
                                    <tr>
                                        <td>{{  $loop->index+1}} </td>
                                        <td><a href="{{route('event.show',$event)}}">{{ $event->title }}</a></td>
                                        <td>
                                            {{$event->address}}
                                        </td>
                                        <td>{{ $event->start_date }}</td>
                                        <td>{{ $event->user->name }}</td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                <br>

            </div>
            <div class="col-md-8">
                @if(count($pastEvents) ===0)
                    <div class="alert alert-warning text-capitalize text-center">
                        Empty Upcommig Events
                    </div>
                @else
                <div class="card bg-secondary">
                    <div class="card-header">{{ __(' Past Event Lists') }}</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Address</th>
                                <th>Start Date</th>
                                <th>By</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($pastEvents as $event)
                                <tr>
                                    <td>{{  $loop->index+1}} </td>
                                    <td><a href=" ">{{ $event->title }}</a></td>
                                    <td>
                                            {{  $event->address }}
                                      </td>
                                    <td>{{ $event->start_date }}</td>
                                    <td>{{ $event->user->name }}</td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
                <br>

            </div>
        </div>
    </div>
    @stop
