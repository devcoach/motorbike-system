@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">


            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Show ') }} {{ $event->name }}</div>
                    <div class="card-body">
                        <p class="text-left">
                            Description:
                        </p>
                        <p>
                            {!! $event->description !!}
                        </p>
                    </div>
                    <div id="map"></div>
                    <table class="table table-bordered mt-3">

                        <tbody>
                        <tr>
                            <th class="table-active">Start date</th>
                            <td>{{ $event->start_date }}</td>
                        </tr>
                        <tr>
                            <th class="table-active">End date</th>
                            <td>{{ $event->end_date }}</td>
                        </tr>
                        <tr>
                            <th class="table-active">Address</th>
                            <td>{{ $event->address }}</td>
                        </tr>
                        <tr>
                            <th class="table-active">By</th>
                            <td>{{ $event->user->name }}</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <br>
            </div>

        </div>
    </div>
@endsection

@section('header-style')
    <style>
        #map {
            width: 100%;
            border: 1px dashed #333;
            height: 300px;
        }
    </style>
@endsection
@section('footer-script')
    <script>
        //// show maps
        function getMaps() {
            var canvas = $('#map');
            var mapOptions = {
                center: new google.maps.LatLng({{$event->lat}}, {{$event->long}}),
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(canvas[0], mapOptions);
        }

        // getMaps();
    </script>
    <script async
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdkY5JMQymRr-LDW4kpAfO8IKEUmD_ZnI&?callback=getMaps"></script>
@endsection
