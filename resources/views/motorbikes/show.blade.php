@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">


            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Show ') }} {{ $motorbike->model }}</div>

                    <div class="card-body">

                        <ul  class="list-group motorbike-group">
                            <li class="list-item">
                            {{ $motorbike->model }}
                            </li>
                            <li class="list-item">
                               <div class="" style="width: 40px;height: 40px; background: {{   $motorbike->color }}"></div>
                            </li>
                            <li class="list-item">
                                {{ $motorbike->weight }} Kg
                            </li>
                            <li class="list-item">
                                {{ $motorbike->price }} Rial
                            </li>
                           <li>
                               <figure class="figure">
                                   <img style="width: 300px;height: 250px;" src="/storage/motorbikes/{{ $motorbike->image}}" class="img-circle" alt=" {{ $motorbike->model }}">
                               </figure>
                           </li>
                        </ul>
                    </div>
                </div>
                <br>
            </div>

        </div>
    </div>
    @stop
