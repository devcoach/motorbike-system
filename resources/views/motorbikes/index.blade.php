@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">


            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __(' Motorbike Lists') }}</div>

                    <div class="card-body">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Model</th>
                                    <th>Color</th>
                                    <th>Weight</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($motorbikes as $motorbike)
                                <tr>
                                    <td>{{  $loop->index+1}} </td>
                                    <td><a href="{{ route('motorbike.show',$motorbike) }}">{{ $motorbike->model }}</a></td>
                                    <td>
                                        <p class="badge " style="color:{{ $motorbike->color }};width: 30px;height: 30px;display: block">
                                            <a href="{{ url('/') }}?sortByColor={{  $motorbike->color }}">

                                            {{ $motorbike->color }}
                                            </a>
                                        </p></td>
                                    <td>{{ $motorbike->weight }}</td>
                                    <td>{{ $motorbike->price }}</td>

                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <div class="text-center"> {{ $motorbikes->appends(['sortByColor' => Request::input('sortByColor')])->links()}}</div>
            </div>

        </div>
    </div>
    @stop
