@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">


                        <div class="col-md-8">
                            @if(session()->has('message')))
                              <div class="alert text-center alert-success">
                                  {{ session('message') }}
                              </div>
                            @endif
                            <div class="card">
                                <div class="card-header">{{ __('Register Motorbike') }}</div>

                                <div class="card-body">
                                    <form method="POST" action="{{ route('motorbike.store') }}" aria-label="{{ __('Register Motorbike') }}" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group row">
                                            <label for="model" class="col-md-4 col-form-label text-md-right">{{ __('Model') }}</label>

                                            <div class="col-md-6">
                                                <input id="model" type="text" class="form-control{{ $errors->has('model') ? ' is-invalid' : '' }}" name="model" value="{{ old('model') }}"  autofocus>

                                                @if ($errors->has('model'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('model') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="color" class="col-md-4 col-form-label text-md-right">{{ __('Color') }}</label>

                                            <div class="col-md-6">
                                                <select name="color" id="color" class="form-control">
                                                    <option disabled>Plaese Select a Color</option>
                                                    <option value="red">Red</option>
                                                    <option value="black">Black</option>
                                                    <option value="yellow">yellow</option>
                                                </select>
                                                @if ($errors->has('color'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="weight" class="col-md-4 col-form-label text-md-right">{{ __('Weight') }}</label>

                                            <div class="col-md-6">
                                                <input id="weight" type="number" class="form-control{{ $errors->has('weight') ? ' is-invalid' : '' }}" name="weight" required>

                                                @if ($errors->has('weight'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>

                                            <div class="col-md-6">
                                                <input id="price" type="number" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" required>

                                                @if ($errors->has('price'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>

                                            <div class="col-md-6">
                                                <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" required>

                                                @if ($errors->has('image'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Register Motorbike') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

    </div>
</div>
@endsection
