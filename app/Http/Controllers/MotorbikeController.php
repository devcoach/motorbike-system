<?php

namespace App\Http\Controllers;

use App\Motorbike;
use foo\bar;
use Illuminate\Http\Request;

class MotorbikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $url=$request->fullUrl();
        $param=$request->input('sortByColor');
        $motobikes = Motorbike::orderBy('created_at', 'DESC')->paginate(5);
        return view('motorbikes.index', ['motorbikes'=>$motobikes, 'url'=>$url, 'param'=>$param]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'model'=>'required|string|max:255',
                'color'=>'required|',
                'weight'=>'required|integer|max:250',
                'price'=>'required|integer|max:250',
                'image'=>'required|image|mimes:jpg,jpeg,png,gif|max:2048',
        ]);
        $name=$request->file('image')->getClientOriginalName();
        $request->file('image')->storeAs('motorbikes', $name);
        $motorbike = new Motorbike([
            'model'=>$request->input('model') ,
            'color'=>$request->input('color') ,
            'weight'=>$request->input('weight') ,
            'price'=>$request->input('price') ,
            'image'=>$name
        ]);
        $motorbike->save();

        session()->flash('message', 'Motorbike has been created');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Motorbike  $motorbike
     * @return \Illuminate\Http\Response
     */
    public function show(Motorbike $motorbike)
    {
        //

        return view('motorbikes.show', compact('motorbike'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Motorbike  $motorbike
     * @return \Illuminate\Http\Response
     */
    public function edit(Motorbike $motorbike)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Motorbike  $motorbike
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Motorbike $motorbike)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Motorbike  $motorbike
     * @return \Illuminate\Http\Response
     */
    public function destroy(Motorbike $motorbike)
    {
        //
    }
}
