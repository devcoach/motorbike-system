<?php

namespace App\Http\Controllers\Event;

use App\Modules\Event\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'show']]);
    }

    public function index()
    {
        $tody = Carbon::today()->format('Y-m-d');
        $upcomingEvents = Event::where('end_date', '>', $tody)->orderBy('start_date', 'desc')->get();
        $pastEvents = Event::where('end_date', '<', $tody)->orderBy('start_date', 'desc')->limit(3)->get();

        return view('events.index', compact('upcomingEvents', 'pastEvents'));
    }

    public function show(Event $event)
    {
        return view('events.show')->with('event', $event);
    }

    public function create()
    {
        return 'crate view';
    }
}
