<?php

namespace App\Modules\Event;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded=[];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public  $timestamps=false;
}
