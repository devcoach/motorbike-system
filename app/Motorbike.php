<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motorbike extends Model
{
    protected $guarded=[];
    protected $table='motorbikes';
    protected $fillable=[
      'model',
      'color',
      'weight',
      'price',
      'image'
    ];
}
